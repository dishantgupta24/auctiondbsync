package com.cars24.dealerengine.dbsync.rmq;

import java.util.*;

public class RmqMessageParser {

    public static Map<String, String> getTableData(List<String> attributes, String message) throws RmqMessageParseException {
        Map<String, String> tableData = new HashMap<>();
        List<String> values = getValues(message);
        for(int i=0; i<attributes.size(); i++) {
            tableData.put(attributes.get(i), values.get(i));
        }
        tableData = cleanTableData(tableData);
        if (attributes.size() != values.size()) {
            throw new RmqMessageParseException(
                    String.format("mismatch in data [%s] and schema [%s] for table ", message, attributes));
        }
        return tableData;
    }

    private static List<String> getValues(String message) throws RmqMessageParseException {
        List<String> attributes = new ArrayList<String>();
        List<String> cleanMessage = new ArrayList<String>();
        for(String item: Arrays.asList(message.split(","))) {
            String[] temp = item.split("\"");
            try {
                if (temp.length == 0){
                    item = "";
                } else {
                    item = temp[1];
                }
                cleanMessage.add(item);
            } catch (Exception exception){
                throw new RmqMessageParseException(exception);
            }
        }
        for (int i=6; i<cleanMessage.size(); i++){
            attributes.add(cleanMessage.get(i));
        }
        return attributes;
    }

    private static Map<String, String> cleanTableData(Map<String, String> tableData) {
        String volt_id = null;
        String keyToPop = null;
        for (Map.Entry<String, String> entry : tableData.entrySet())
        {
            String key = entry.getKey();
            if (key.startsWith("id_")) {
                volt_id = entry.getValue();
                keyToPop = key;
            }
        }
        tableData.remove(keyToPop);
        tableData.put("volt_db_id", volt_id);
        return tableData;
    }
}
