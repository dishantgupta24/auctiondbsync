package com.cars24.dealerengine.dbsync.rmq;

import com.cars24.dealerengine.dbsync.BaseException;

public class RmqMessageParseException extends BaseException{

    public RmqMessageParseException(Exception exception) {
        super(exception);
    }

    public RmqMessageParseException(String message) {
        super(message);
    }
}
