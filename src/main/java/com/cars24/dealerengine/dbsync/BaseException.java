package com.cars24.dealerengine.dbsync;

public class BaseException extends Exception{

    private String message;
    private String originalExceptionName;
    private StackTraceElement[] originalExceptionStacktrace;

    public BaseException(String mesage) {
        this.message = mesage;
    }

    public BaseException(Exception exception) {
        this.message = exception.getMessage();
        this.originalExceptionName = exception.getClass().getName();
        this.originalExceptionStacktrace = exception.getStackTrace();
    }

    @Override
    public String toString() {
        return super.toString();
    }

    @Override
    public String getMessage() {
        return message;
    }

    public String getOriginalExceptionName() {
        return originalExceptionName;
    }

    public StackTraceElement[] getOriginalExceptionStacktrace() {
        return originalExceptionStacktrace;
    }
}
