package com.cars24.dealerengine.dbsync;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppMain {

    public static void main(String[] args) throws InterruptedException {
        SpringApplication.run(AppMain.class, args);
    }
}
