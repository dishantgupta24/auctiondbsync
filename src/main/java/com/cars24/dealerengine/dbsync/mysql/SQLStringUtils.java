package com.cars24.dealerengine.dbsync.mysql;

import java.util.Collection;
import java.util.Map;

public class SQLStringUtils {
    public static String quote(String message){
        return String.format("'%s'", message);
    }

    public static String getCommaSeparatedSQLValues(Collection<String> values) {
        StringBuffer valuesString = new StringBuffer();
        values.forEach(value -> {
            valuesString.append("'" + value + "',");
        });
        valuesString.deleteCharAt(valuesString.length()-1);
        return valuesString.toString();
    }

    public static String getOnDuplicateSQLUpdateAttrs(Map<String, String> rowData) {
        StringBuffer updateAttrs = new StringBuffer();
        rowData.keySet().forEach(key->{
            updateAttrs.append(key + "='" + rowData.get(key) + "',");
        });
        updateAttrs.deleteCharAt(updateAttrs.length()-1);
        return updateAttrs.toString();
    }
}
