package com.cars24.dealerengine.dbsync.mysql;

import com.cars24.dealerengine.dbsync.BaseException;

public class MySqlException extends BaseException {

    public MySqlException(Exception exception) {
        super(exception);
    }
}
