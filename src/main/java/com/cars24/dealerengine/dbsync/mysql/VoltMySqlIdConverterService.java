package com.cars24.dealerengine.dbsync.mysql;

import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;

import static com.cars24.dealerengine.dbsync.mysql.SQLStringUtils.quote;

@Service
public class VoltMySqlIdConverterService {


    @PersistenceContext
    EntityManager entityManager;


    public String convert(String table, String columnName, String voltId) {
        String query = String.format("SELECT %s from %s where volt_id=%s", columnName, table, quote(voltId));
        ArrayList<String> results = (ArrayList<String>) entityManager.createNativeQuery(query).getResultList();
        if (results.size()>0){
            return results.get(0);
        }
        return null;
    }
}
