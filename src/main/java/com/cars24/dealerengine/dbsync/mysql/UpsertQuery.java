package com.cars24.dealerengine.dbsync.mysql;

import org.apache.commons.lang3.StringUtils;

import java.util.Map;

public class UpsertQuery {

    public static final String UPSERT = "INSERT into %s (%s) VALUES (%s) ON DUPLICATE KEY UPDATE %s";

    public static String makeQuery(String tableName, final Map<String, String> rowData){
        String finalQuery = String.format(
                UPSERT,
                tableName,
                StringUtils.join(rowData.keySet(), ','),
                SQLStringUtils.getCommaSeparatedSQLValues(rowData.values()),
                SQLStringUtils.getOnDuplicateSQLUpdateAttrs(rowData)
        );
        return finalQuery;
    }
}
