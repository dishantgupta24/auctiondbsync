package com.cars24.dealerengine.dbsync.services;

import com.cars24.dealerengine.dbsync.AppConfig;
import com.cars24.dealerengine.dbsync.mysql.UpsertQuery;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Service
public class CarPriceService {

    @PersistenceContext
    EntityManager entityManager;

    public List<String> getAttributes(){
        return Arrays.asList(AppConfig.CarPrice.TABLE_ATTRS.split(","));
    }

    public String getTableName() {
        return AppConfig.CarPrice.TABLE_NAME;
    }

    public String getQueueName() {
        return AppConfig.CarPrice.QUEUE_NAME;
    }

    @Transactional
    public void execute(Map<String, String> rowData) {
        String query = UpsertQuery.makeQuery(getTableName(), rowData);
        entityManager.createNativeQuery(query).executeUpdate();
    }
}