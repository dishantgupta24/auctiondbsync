package com.cars24.dealerengine.dbsync.services;

import com.cars24.dealerengine.dbsync.AppConfig;
import com.cars24.dealerengine.dbsync.mysql.UpsertQuery;
import com.cars24.dealerengine.dbsync.mysql.VoltMySqlIdConverterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Service
public class BidService {

    @PersistenceContext
    EntityManager entityManager;

    @Autowired
    VoltMySqlIdConverterService voltMySqlIdConverter;

    @Autowired
    AuctionService auctionService;

    public List<String> getAttributes(){
        return Arrays.asList(AppConfig.Bid.TABLE_ATTRS.split(","));
    }

    public String getTableName() {
        return AppConfig.Bid.TABLE_NAME;
    }

    public String getQueueName() {
        return AppConfig.Bid.QUEUE_NAME;
    }

    @Transactional
    public void execute(Map<String, String> rowData) throws Exception {
        String query = UpsertQuery.makeQuery(getTableName(), rowData);

        // replace volt ids by mysql ids for foreign key fk_app_auction -> app_auction
        String mysqlId = voltMySqlIdConverter.convert(
                auctionService.getTableName(), "id_app_auction", rowData.get("fk_app_auction"));
        if (mysqlId == null){
            throw new Exception("Unable to find foreign key relation");
        }
        rowData.put("fk_app_auction", mysqlId);
        entityManager.createNativeQuery(query).executeUpdate();
    }
}