package com.cars24.dealerengine.dbsync.services;

import com.cars24.dealerengine.dbsync.AppConfig;
import com.cars24.dealerengine.dbsync.mysql.MySqlException;
import com.cars24.dealerengine.dbsync.mysql.UpsertQuery;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Service
public class AuctionService {

    @PersistenceContext
    EntityManager entityManager;

    public List<String> getAttributes(){
        return Arrays.asList(AppConfig.Auction.TABLE_ATTRS.split(","));
    }

    public String getTableName() {
        return AppConfig.Auction.TABLE_NAME;
    }

    public String getQueueName() {
        return AppConfig.Auction.QUEUE_NAME;
    }

    @Transactional
    public void execute(Map<String, String> rowData) throws MySqlException {
        String query = UpsertQuery.makeQuery(getTableName(), rowData);
        try {
            entityManager.createNativeQuery(query).executeUpdate();
        } catch (Exception exception) {
            throw new MySqlException(exception);
        }
    }
}