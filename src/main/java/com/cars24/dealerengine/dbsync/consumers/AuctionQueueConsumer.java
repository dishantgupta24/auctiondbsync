package com.cars24.dealerengine.dbsync.consumers;

import com.cars24.dealerengine.dbsync.AppConfig;
import com.cars24.dealerengine.dbsync.mysql.MySqlException;
import com.cars24.dealerengine.dbsync.rmq.RmqMessageParseException;
import com.cars24.dealerengine.dbsync.rmq.RmqMessageParser;
import com.cars24.dealerengine.dbsync.services.AuctionService;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class AuctionQueueConsumer implements QueueConsumer{

    @Autowired
    AuctionService auctionService;

    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(value = AppConfig.Auction.QUEUE_NAME, durable = "true"),
            exchange = @Exchange(value = "${rmq.exchange.name}"))
    )
    public void receiveMessage(String message) {
        try {
            Map<String, String> rowData = RmqMessageParser.getTableData(
                    auctionService.getAttributes(), message);
            auctionService.execute(rowData);
        } catch (MySqlException | RmqMessageParseException exception){
            System.out.println(
                    String.format(
                            "%s-%s-%s",
                            exception.getOriginalExceptionName(), exception.getMessage(), exception.getStackTrace().toString()
                    )
            );
        }
    }
}
