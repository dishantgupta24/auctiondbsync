package com.cars24.dealerengine.dbsync.consumers;

import com.cars24.dealerengine.dbsync.AppConfig;
import com.cars24.dealerengine.dbsync.rmq.RmqMessageParseException;
import com.cars24.dealerengine.dbsync.rmq.RmqMessageParser;
import com.cars24.dealerengine.dbsync.services.BiddingQuotationService;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class BiddingQuotationQueueConsumer implements QueueConsumer{

    @Autowired
    BiddingQuotationService biddingQuotationService;

    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(value = AppConfig.BidQuotation.QUEUE_NAME, durable = "true"),
            exchange = @Exchange(value = "${rmq.exchange.name}"))
    )
    public void receiveMessage(String message) throws RmqMessageParseException {
        Map<String, String> rowData = RmqMessageParser.getTableData(
                biddingQuotationService.getAttributes(), message);
        biddingQuotationService.execute(rowData);
    }
}