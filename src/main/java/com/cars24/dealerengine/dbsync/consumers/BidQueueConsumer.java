package com.cars24.dealerengine.dbsync.consumers;

import com.cars24.dealerengine.dbsync.AppConfig;
import com.cars24.dealerengine.dbsync.rmq.RmqMessageParser;
import com.cars24.dealerengine.dbsync.services.BidService;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class BidQueueConsumer implements QueueConsumer{

    @Autowired
    BidService bidService;

    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(value = AppConfig.Bid.QUEUE_NAME, durable = "true"),
            exchange = @Exchange(value = "${rmq.exchange.name}"))
    )
    public void receiveMessage(String message) throws Exception {
        Map<String, String> rowData = RmqMessageParser.getTableData(
                bidService.getAttributes(), message);
        bidService.execute(rowData);
    }
}