package com.cars24.dealerengine.dbsync.consumers;

public interface QueueConsumer {

    public void receiveMessage(String message) throws Exception;
}

