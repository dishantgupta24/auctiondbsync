package com.cars24.dealerengine.dbsync;

public class AppConfig {

    public static final String exchangeName = "auctiondbsync";

    public static class Test {
        public static final String QUEUE_NAME = "1234";
        public static final String TABLE_NAME = "towns";
        public static final String TABLE_ATTRS = "town,county,state";
    }

    public static class Auction {
        public static final String QUEUE_NAME = "APP_AUCTION_MYSQL";
        public static final String TABLE_NAME = "app_auction";
        public static final String TABLE_ATTRS = "id_app_auction,carId,carCode,appointment_id,auction_bidding_status,auction_start_time,auction_end_time,grace_period_bidding_time_increment,grace_time_counter,auction_type,auction_mode,auction_status,highest_bidder,cep,rp,fmv,tp,buy_now_price,car_bought,car_bought_price,car_bought_date,pause_time,interval_after_pause,utm_source,created_by,created_date,updated_by,updated_date,auction_count,auction_buynow,b2b,is_inventory,is_scrap,category,c24Quote,startAuction,reason,c24quote_expiry_time,c24QuoteTag,oneClickBuy,expectedOneClickAmount,oneClickBuyType,is_inventory_buying,inventory_buying_rp,inventory_buying_im,fk_app_inventory_buying,is_manual_rp,manual_rp_update_at,manual_rp_update_by,inventory_buying_orp,instant_buy,in_transit_inventory_id";
    }

    public static class Bid {
        public static final String QUEUE_NAME = "APP_BID_MYSQL";
        public static final String TABLE_NAME = "app_bid";
        public static final String TABLE_ATTRS = "id_app_bid,fk_app_auction,carId,appointment_id,fk_dealer_id,bid_amount,bid_time,auction_type,is_higher_bid,is_latest_bid,bid_status,grace_time_counter,cep,rejected,rejection_reason,rejected_by,rejected_at,suspended,suspeneded_date,verified_date,verified_by,is_winner,utm_source,sales_verified,winning_date,verified_reason,bid_by,updated_by,unflag_reason,updated_date,is_anomaly,nulify_reason,is_buy_now,nulify,nulify_date,suspended_reason,dealer_ip,original_highest_bidder,is_returned,is_auto_bid,transportation_charge,retail_transportation_charge,step_rate,bid_time_millisecond,user_device_id";
    }

    public static class BidQuotation {
        public static final String QUEUE_NAME = "APP_BIDDING_QUOTATION_MYSQL";
        public static final String TABLE_NAME = "app_bidding_quotation";
        public static final String TABLE_ATTRS = "id_app_bidding_quotation,dealer_id,car_id,appointment_id,fk_app_auction,quote_amount,status,created_date,updated_date,is_latest,is_outbid,user_device_id,is_normal_bid,is_upcoming_bid";
    }

    public static class CarPrice {
        public static final String QUEUE_NAME = "APP_CAR_PRICE_MYSQL";
        public static final String TABLE_NAME = "app_car_price";
        public static final String TABLE_ATTRS = "id_app_car_price,carId,appointment_id,cep_current,cep_previous,rp,manual_inventory_buying_rp,ipp,fmv,tp,created_by,created_at,updated_at";
    }
}
